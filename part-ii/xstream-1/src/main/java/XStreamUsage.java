import com.thoughtworks.xstream.XStream;

public class XStreamUsage {

    private String string = "Sample string field";

    private int integer = 1337;

    public static void main(String[] args) {
        XStream xStream = new XStream();

        System.out.println(xStream.toXML(new XStreamUsage()));
    }

}
