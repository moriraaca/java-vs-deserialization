import org.apache.tomcat.util.codec.binary.Base64;

import java.io.*;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
        name = "Servlet",
        urlPatterns = {"/"}
    )
public class Servlet extends HttpServlet {

    private static final SecretKeySpec keySpec = new SecretKeySpec("3BaUHxi9OBp2FtnPipB9OZxehd7O5UDxvJYxdNwSk9I6sXnEbTQJSh5H2Y988VU".getBytes(), "HmacSHA256");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();

        Data data = null;

        if (null != cookies) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("data")) {
                    try {
                        byte[] serialized = Base64.decodeBase64(verifyAndGetCookie(cookie.getValue()));
                        ByteArrayInputStream bais = new ByteArrayInputStream(serialized);
                        ObjectInputStream ois = new ObjectInputStream(bais);
                        data = (Data) ois.readObject();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        if (null == data) {
            data = new Data("Anonymous");
        }

        request.setAttribute("name", data.getName());
        request.getRequestDispatcher("page.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (null != request.getParameter("name")) {
            Data data = new Data(request.getParameter("name"));

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(data);

            Cookie cookie = new Cookie("data", signCookie(Base64.encodeBase64String(baos.toByteArray())));
            response.addCookie(cookie);
        }

        response.sendRedirect("/");
    }

    private String verifyAndGetCookie(String cookie) throws ServletException {
        String [] parts = cookie.split("\\.");

        if (parts.length != 2) {
            throw new ServletException("Malformed cookie!");
        }

        try {
            String b64value = parts[0];
            String b64mac = parts[1];

            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(keySpec);

            // MessageDigest.isEqual is constant-time in recent Java versions
            if (!MessageDigest.isEqual(mac.doFinal(Base64.decodeBase64(b64value)), Base64.decodeBase64(b64mac))) {
                throw new ServletException("Malformed cookie!");
            }

            return b64value;
        } catch (NoSuchAlgorithmException e) {
            throw new ServletException("MAC algorithm not found");
        } catch (InvalidKeyException e) {
            throw new ServletException("Bad key spec");
        }
    }

    private String signCookie(String cookie) throws ServletException {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(keySpec);

            String sig = Base64.encodeBase64String(mac.doFinal(Base64.decodeBase64(cookie.getBytes())));

            return cookie + '.' + sig;
        } catch (NoSuchAlgorithmException e) {
            throw new ServletException("MAC algorithm not found");
        } catch (InvalidKeyException e) {
            throw new ServletException("Bad key spec");
        }
    }
}